@extends('layouts.app')
@section('content')

    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            {{ Form::open(array('route'=>'articles.store')) }}
                @include('articles.form_master')
            {{ Form::close() }}
        </div>
    </div>

@endsection