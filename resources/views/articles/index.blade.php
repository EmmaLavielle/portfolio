@extends('layouts.app')
@section('content')

    <div class="row">
        <div class="col-sm-12">
            <div class="full-right">
            <h2>Articles</h2>
            </div>
        </div>
    </div>

    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif

    <table class="table table-bordered">
        <tr>
            <th>Titre</th>
            <th>Description</th>
            <th>Image_url</th>
            <th>Utilisateur</th>
            <th class="text-center">
                <a class="btn btn-success btn-sm" href="{{ route('articles.create') }}">
                    <i class="glyphicon glyphicon-plus"></i>
                </a>
            </th>
        </tr>
        @foreach ($articles as $article)
            <tr>
                <td>{{ $article->title }}</td>
                <td>{{ $article->description }}</td>
                <td>{{ $article->image_url }}</td>
                <td>{{ $article->user_id }}</td>
                <td>
                    <a class="btn btn-info btn-sm" href="{{ route('articles.show', $article->id) }}">
                        <i class="glyphicon glyphicon-th-large"></i>
                    </a>
                    <a class="btn btn-primary btn-sm" href="{{ route('articles.edit', $article->id) }}">
                        <i class="glyphicon glyphicon-pencil"></i>
                    </a>
                    {!! Form::open(['method' => 'DELETE', 'route' => ['articles.destroy', $article->id],'style'=>'display:inline']) !!}
                        <button type="submit" style="display: inline;" class="btn btn-danger btn-sm"><i class="glyphicon glyphicon-trash"></i></button>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
    </table>

@endsection