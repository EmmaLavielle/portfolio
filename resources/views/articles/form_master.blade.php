<div class="row">
    <div class="col-sm-2">
        {{ form::label('title', 'Titre') }}
    </div>
    <div class="col-sm-10">
        <div class="form-group {{ $errors->has('title') ? 'has-error' : "" }}">
            {{ Form::text('title', NULL, ['class'=>'form-control', 'id'=>'title', 'placeholder'=>"Titre de l'article"]) }}
            {{ $errors->first('title', ':message') }}
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-2">
        {{ form::label('description', 'Description') }}
    </div>
    <div class="col-sm-10">
        <div class="form-group {{ $errors->has('description') ? 'has-error' : "" }}">
            {{ Form::text('description', NULL, ['class'=>'form-control', 'id'=>'description', 'placeholder'=>"Description de l'article"]) }}
            {{ $errors->first('description', ':message') }}
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-2">
        {{ form::label('image_url', 'Image url') }}
    </div>
    <div class="col-sm-10">
        <div class="form-group {{ $errors->has('image_url') ? 'has-error' : "" }}">
            {{ Form::text('image_url', NULL, ['class'=>'form-control', 'id'=>'image_url', 'placeholder'=>"Image url de l'article"]) }}
            {{ $errors->first('image_url', ':message') }}
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-2">
        {{ form::label('user_id', 'Utilisateur') }}
    </div>
    <div class="col-sm-10">
        <div class="form-group {{ $errors->has('user_id') ? 'has-error' : "" }}">
            {{ Form::text('user_id', NULL, ['class'=>'form-control', 'id'=>'user_id', 'placeholder'=>"Créateur de l'article"]) }}
            {{ $errors->first('user_id', ':message') }}
        </div>
    </div>
</div>

<div class="form-group">
    {{ Form::button(isset($model)? 'Update' : 'save', ['class'=>'btn btn-success', 'type'=>'submit']) }}
</div>