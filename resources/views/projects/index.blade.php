@extends('layouts.app')
@section('content')

    <div class="row">
        <div class="col-sm-12">
            <div class="full-right">
            <h2>Projets</h2>
            </div>
        </div>
    </div>

    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif

    <table class="table table-bordered">
        <tr>
            <th>Nom</th>
            <th>Description</th>
            <th>Image_url</th>
            <th>Technologie</th>
            <th>Repo_url</th>
            <th>Website_url</th>
            <th>Catégorie</th>
            <th class="text-center">
                <a class="btn btn-success btn-sm" href="{{ route('projects.create') }}">
                    <i class="glyphicon glyphicon-plus"></i>
                </a>
            </th>
        </tr>
        @foreach ($projects as $project)
            <tr>
                <td>{{ $project->name }}</td>
                <td>{{ $project->description }}</td>
                <td>{{ $project->image_url }}</td>
                <td>{{ $project->technology }}</td>
                <td>{{ $project->repo_url }}</td>
                <td>{{ $project->website_url }}</td>
                <td>{{ $project->category_id }}</td>
                <td>
                    <a class="btn btn-info btn-sm" href="{{ route('projects.show', $project->id) }}">
                        <i class="glyphicon glyphicon-th-large"></i>
                    </a>
                    <a class="btn btn-primary btn-sm" href="{{ route('projects.edit', $project->id) }}">
                        <i class="glyphicon glyphicon-pencil"></i>
                    </a>
                    {!! Form::open(['method' => 'DELETE','route' => ['projects.destroy', $project->id],'style'=>'display:inline']) !!}
                        <button type="submit" style="display: inline;" class="btn btn-danger btn-sm"><i class="glyphicon glyphicon-trash"></i></button>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
    </table>

@endsection