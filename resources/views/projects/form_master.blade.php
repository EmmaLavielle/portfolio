<div class="row">
    <div class="col-sm-2">
        {{ form::label('name', 'Nom') }}
    </div>
    <div class="col-sm-10">
        <div class="form-group {{ $errors->has('name') ? 'has-error' : "" }}">
            {{ Form::text('name', NULL, ['class'=>'form-control', 'id'=>'name', 'placeholder'=>'Nom du projet']) }}
            {{ $errors->first('name', ':message') }}
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-2">
        {{ form::label('description', 'Description') }}
    </div>
    <div class="col-sm-10">
        <div class="form-group {{ $errors->has('description') ? 'has-error' : "" }}">
            {{ Form::text('description', NULL, ['class'=>'form-control', 'id'=>'description', 'placeholder'=>'Description du projet']) }}
            {{ $errors->first('description', ':message') }}
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-2">
        {{ form::label('image_url', 'Image url') }}
    </div>
    <div class="col-sm-10">
        <div class="form-group {{ $errors->has('image_url') ? 'has-error' : "" }}">
            {{ Form::text('image_url', NULL, ['class'=>'form-control', 'id'=>'image_url', 'placeholder'=>'Image url du projet']) }}
            {{ $errors->first('image_url', ':message') }}
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-2">
        {{ form::label('technology', 'Technologie') }}
    </div>
    <div class="col-sm-10">
        <div class="form-group {{ $errors->has('technology') ? 'has-error' : "" }}">
            {{ Form::text('technology', NULL, ['class'=>'form-control', 'id'=>'technology', 'placeholder'=>'Technologie du projet']) }}
            {{ $errors->first('technology', ':message') }}
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-2">
        {{ form::label('repo_url', 'Repo_url') }}
    </div>
    <div class="col-sm-10">
        <div class="form-group {{ $errors->has('repo_url') ? 'has-error' : "" }}">
            {{ Form::text('repo_url', NULL, ['class'=>'form-control', 'id'=>'repo_url', 'placeholder'=>'Repo_url du projet']) }}
            {{ $errors->first('repo_url', ':message') }}
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-2">
        {{ form::label('website_url', 'Website_url') }}
    </div>
    <div class="col-sm-10">
        <div class="form-group {{ $errors->has('website_url') ? 'has-error' : "" }}">
            {{ Form::text('website_url', NULL, ['class'=>'form-control', 'id'=>'website_url', 'placeholder'=>'Website_url du projet']) }}
            {{ $errors->first('website_url', ':message') }}
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-2">
        {{ form::label('category_id', 'Catégorie') }}
    </div>
    <div class="col-sm-10">
        <div class="form-group {{ $errors->has('category_id') ? 'has-error' : "" }}">
            {{ Form::text('category_id', NULL, ['class'=>'form-control', 'id'=>'category_id', 'placeholder'=>'Catégorie du projet']) }}
            {{ $errors->first('category_id', ':message') }}
        </div>
    </div>
</div>

<div class="form-group">
    {{ Form::button(isset($model)? 'Update' : 'save', ['class'=>'btn btn-success', 'type'=>'submit']) }}
</div>