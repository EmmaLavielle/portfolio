<?php

use Illuminate\Database\Seeder;

class ProjectSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('projects')->insert([
            [
                'id'=> 1,
                'name' => "a",
                'description' => "a",
                'image_url' => "a",
                'technology' => "a",
                'repo_url' => "a",
                'website_url' => "a",
                'category_id' => "a",
            ]
        ]);
    }
}
