<?php

use Illuminate\Database\Seeder;

class ArticleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('articles')->insert([
            [
                'id'=> 1,
                'title' => "a",
                'description' => "a",
                'image_url' => "a",
                'user_id' => "a",
            ]
        ]);
    }
}
