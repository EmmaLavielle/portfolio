<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    protected $fillable = [
        'name',
        'description',
        'image_url',
        'technology',
        'repo_url',
        'website_url',
        'category_id',
    ];
}
